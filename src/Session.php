<?php

namespace GranitSDK\Service;

use Phalcon\Http\Request;

class Session extends \Phalcon\Session\Manager
{
	public function start(): bool
	{
		if ($this->exists()) {
			return true;
		}

		if (headers_sent()) {
			return false;
		}

		set_error_handler(array($this, 'handleError'));
		$res = session_start();
		restore_error_handler();

		return $res;
	}

	protected function handleError ($errno, $errstr, $errfile, $errline)
	{
		$log = [
			'message' => 'session start error: ' . $errstr,
			'errno' => $errno,
			'severity' => 'ERROR',
			'httpRequest' => [
				'requestMethod' => $_SERVER['REQUEST_METHOD'] ?? '',
				'requestBody' => file_get_contents('php://input'),
				'requestUrl' => $_SERVER['REQUEST_URI'] ?? '',
				'requestQuery' => $_SERVER['QUERY_STRING'] ?? '',
				'userAgent' => $_SERVER['HTTP_USER_AGENT'] ?? '',
				'remoteIp' => $_SERVER['REMOTE_ADDR'] ?? '',
			],
		];

		$sid = isset($_COOKIE['PHPSESSID']) ? $_COOKIE['PHPSESSID'] : 'empty';

		$mem = new \Memcached();
		$mem->addServer('memcached-session-1', 11211);
		$result = $mem->get('memc.sess.' . $sid);

		$log['memcached-session-1'] = [
			'message' => $mem->getResultMessage(),
			'code' => $mem->getResultCode(),
			'result' => $result,
			'sessid' => $sid
		];


		$mem = new \Memcached();
		$mem->addServer('memcached-session-2', 11211);
		$result = $mem->get('memc.sess.' . $sid);

		$log['memcached-session-1'] = [
			'message' => $mem->getResultMessage(),
			'code' => $mem->getResultCode(),
			'result' => $result,
			'sessid' => $sid
		];


		file_put_contents('php://stdout', json_encode($log) . "\n");
	}

	public function __construct()
	{
		if ($this->hasIdInRequest()) {
			$this->setId($this->getIdFromRequest());
		}

		$this->start();
	}

	protected  function hasIdInRequest()
	{
		$id = $this->getIdFromRequest();

		if (empty($id)) {
			return false;
		}

		if ($id == 'undefined') {
			return false;
		}

		return true;
	}

	protected  function getIdFromRequest()
	{
		/** @var Request $request */
		$request = \Phalcon\Di::getDefault()->get('request');

		if ($request->hasHeader('token')) {
			return $request->getHeader('token');
		}

		if ($request->hasQuery('_session')) {
			return $request->getQuery('_session');
		}

		if ($request->hasPost('_session')) {
			return $request->getPost('_session');
		}
	}
}